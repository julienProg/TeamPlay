var myApp = angular.module('myApp', [
  'ngRoute',
  'ui.calendar'
]);

var socket = io();
var data = {
	user:{},
	users:{},
	teams:{},
};
var selectedTeamIndex;
var selectedChatRoomIndex;