myApp.config(['$locationProvider', '$routeProvider',
  function($locationProvider, $routeProvider) {

    if (typeof data.user.userID === "undefined" && window.location.pathname !== "/login" && window.location.pathname !== "/login/") {
      window.location.replace("/login");
    }

    $routeProvider
      .when('/addMember', {
        template: '<add-member></add-member>'
      })
      .when('/login', {
        template: '<login></login>'
      })
      .when('/dashboard', {
        template: '<dashboard></dashboard>'
      })
      .when('/chat', {
        template: '<chat></chat>'
      })
      .when('/dashboardEquipe', {
        template: '<dashboard-equipe></dashboard-equipe>'
      }).when('/calendar', {
        template: '<calendar></calendar>'
      }).otherwise('/login');

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

  }
]);
