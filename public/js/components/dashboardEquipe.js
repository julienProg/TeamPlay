
myApp.component('dashboardEquipe', {
    templateUrl : '/views/dashboardEquipe.html',
    controller : function () {
		var self = this;
		self.data = "";
		self.menu = true;
		socket.on('setData', function(d) {
		      console.log("d : ");
		      console.log(d);
		      self.data = d;

	      //Il faut faire en sorte de refresh les données sur la page
	    });
		self.babillards = "active";
		self.message = "";
		self.calendrier = "";
		self.membre = "";

		self.boutonCategorie = true;
		self.boutonTache = true;
		
		self.collapseMenu = function(){
			if(self.menu){
				self.menu = false;
				$(".bodyDashboard").css("padding-left","0")
			}else{
				self.menu = true;
				$(".bodyDashboard").css("padding-left","250px")
			}
		}
		
		self.premierClick = false;
		
		self.changeMenu = function(pos){
			self.babillards = "";
			self.message = "";
			self.calendrier = "";
			self.membre = "";
			switch(pos){
				case 1:
					self.babillards = "active";
					break;
				case 2:
					self.message = "active";
					break;
				case 3:
					self.calendrier = "active";
					if(!self.premierClick){
						setTimeout(function(){$("#refresh").click();},100);
						self.premierClick = true;
					}
					
					break;
				case 4:
					self.membre = "active";
					break;
			}
		}
		
		
		self.creerTache = function(){
			console.log("permet de creer un tache d'un categorie")
		}
		
		
		self.categorie = function(){
			
		}
		
    }
});
