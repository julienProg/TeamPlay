myApp.component('dashboard', {
  templateUrl: '/views/dashboard.html',
  controller: function() {
    var self = this;
    self.menu = true;
    self.data = data;

    self.collapseMenu = function() {
      if (self.menu) {
        self.menu = false;
        $(".bodyDashboard").css("padding-left", "0")
      } else {
        self.menu = true;
        $(".bodyDashboard").css("padding-left", "250px")
      }
    }
    socket.on('setData', function(d) {
      data = d;
      self.data = d;
    });
  }
});
