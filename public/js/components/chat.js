var chatRoom;
var chatRoomIndex = 0;
myApp.component('chat', {
  templateUrl: '/views/chat.html',
  controller: function() {
    displayChatRoom();
    bindEnter();
    focusInput();

    function submit() {
      var content = $("#sendMessageInput").val();
      if (!content) return;
      $("#sendMessageInput").val("");
      var chatMessage = {
        userID: data.user.userID,
        content: content
      }
      socket.emit('insertChatMessage', chatMessage, chatRoom.chatRoomID);
    }

    this.submit = submit;
  }
});

socket.on('addChatMessage', function(chatMessage, chatRoomID) {
  if( chatRoom.chatRoomID === chatRoomID){
    chatRoom.chatMessages.push(chatMessage);
    displayChatRoom();
  }
});

function displayChatRoom() {
  var team = data.teams[selectedTeamIndex];
  if (typeof team === "undefined") return;
  if (team.chatRooms.length > 0)
    chatRoom = team.chatRooms[0];
  else
    chatRoom = [];

  var chatDiv = $("#receivedMessages");
  chatDiv.html("");
  var chatMessages = chatRoom.chatMessages;
  if (typeof chatMessages === "undefined") return;
  for (var i = 0; i < chatMessages.length; i++)
    chatDiv.append(formatChatMessage(chatMessages[i]));
  scroll.call(chatDiv, chatDiv[0].scrollHeight, this);
}

function formatChatMessage(chatMessage) {
  var classModifier = chatMessage.userID === data.user.userID ? "me" : "notMe";
  return "<div class='chatMessage " + classModifier + "'>" +
    "<p class='userID'>" + getUserNameFromID(chatMessage.userID) + " <i>(" + formatTimestamp(chatMessage.dateCreated) + ")</i></p>" +
    "<p class='messageContent'>" + chatMessage.content + "</p>" +
    "</div>";
}

function getUserNameFromID(userID) {
  for (var i = 0; i < data.users.length; i++) {
    if (data.users[i].userID === userID)
      return data.users[i].name;
  }
}

function formatTimestamp(timestamp) {
  var d = new Date(timestamp);
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var date = d.getDate();
  var hours = d.getHours();
  var minutes = d.getMinutes();
  if (month < 10) month = "0" + month;
  if (minutes < 10) minutes = "0" + minutes;
  if (isTodaysDate(d))
    return "Aujourd'hui à " + hours + ":" + minutes;
  else
    return year + "-" + month + "-" + date + " à " + hours + ":" + minutes;
}

function scroll(height, ele) {
  this.stop().animate({
    scrollTop: height
  }, 250, function() {
    var dir = height ? "top" : "bottom";
    $(ele).html("scroll to " + dir).attr({
      id: dir
    });
  });
};

function bindEnter() {
  $(document).keypress(function(e) {
    if (e.which == 13) {
      $("#sendMessageButton").click();
    }
  });
}

function focusInput() {
  $("#sendMessageInput").focus();
}

function isTodaysDate(d1) {
  var d2 = new Date();
  return d1.setHours(0, 0, 0, 0) === d2.setHours(0, 0, 0, 0);
}
