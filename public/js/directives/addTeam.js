myApp.directive('addTeam', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/views/addTeam.html',
    link: function(scope, element, attr) {
      scope.team = scope.team || {};
      scope.submit = function() {
        if (typeof scope.team.name !== "string" || scope.team.name.length === 0)
          return alert("Veuillez fournir un nom d'équipe!");
        if (typeof scope.team.description !== "string" || scope.team.description.length === 0)
          return alert("Veuillez fournir une description d'équipe!");
        socket.emit("insertTeam", scope.team, data.user);
        scope.team = {};
      }
      socket.on('setData', function(d) {
        scope.$apply(function() {});
      });
    }
  };
});
