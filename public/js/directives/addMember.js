myApp.directive('addMember', ['$location', function($location) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/views/addMemberForm.html',
    link: function(scope, element, attr) {
      scope.user = scope.user || {};

      scope.submit = function() {
        socket.emit("insertUser", scope.user);
      }

      socket.on('authResponse', function(response) {
        if (response.valid) {
          console.log("replacing path...");
          $location.path("/dashboard").replace();
          scope.$apply();
        } else {
          alert(response.msg);
        }
      });

    }
  };
}]);
