myApp.directive('login', ['$location', function($location) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/views/login.html',
    link: function(scope, element, attr) {
      scope.user = scope.user || {};

      scope.submit = function() {
        socket.emit("authRequest", scope.user);
      }

      socket.on('authResponse', function(response) {
        data = response.data;
        if (response.valid) {
          $location.path("/dashboard").replace();
          scope.$apply();
        } else {
          alert(response.msg);
        }
      });
    }
  };
}]);
