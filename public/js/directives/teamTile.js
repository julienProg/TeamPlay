myApp.directive('teamTile', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: '/views/teamTile.html',
    scope: {
      name: "@",
      description: "@",
      ndashboards: "@",
      nchatrooms: "@",
      ncalendars: "@",
      index: "@",
    },
    link: function(scope, element, attr) {
      scope.user = data.user.name;

      $(".teamTile").unbind().click(function(event) {
        selectedTeamIndex = parseInt($(event.target.closest("a")).attr("team-index"));
        console.log("selectedTeamIndex : ");
        console.log(selectedTeamIndex);
      })
    }
  };
});
