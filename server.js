var express = require('express'); // https://www.npmjs.com/package/express
var fs = require('fs'); // https://nodejs.org/api/fs.html
var http = require('http'); // https://nodejs.org/api/http.html
var https = require('https'); // https://nodejs.org/api/https.html
var mongoose = require('mongoose'); // https://www.npmjs.com/package/mongoose
var socketIO = require('socket.io'); // https://www.npmjs.com/package/socket.io

var app = express();
app.use("/", express.static('public'));
app.use("/", express.static('node_modules'));

app.get('/*', function(req, res) {
  res.sendFile(__dirname + "/index.html");
});

var server = http.createServer(app);
var io = socketIO(server);

server.listen(8080, function() {});

eval("" + fs.readFileSync(__dirname + '/server/mongo.js'));
eval("" + fs.readFileSync(__dirname + '/server/sockets.js'));
eval("" + fs.readFileSync(__dirname + '/server/utility.js'));
