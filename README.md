# TeamPlay

Pour initialiser le projet : 

1. Installer nodeJS (préférablement v6.2.2).

2. Faire un clone du projet.

3. À partir du répertoire du projet, ouvrir une console.

4. Entrer la commande "npm install" et attendre que les modules s'installent.

5. Dans une console avec les permissions d'administrateur, exécuter les commandes "npm install -g grunt".

 
Pour installer MongoDB : 

https://www.mongodb.com/download-center?jmp=nav#community
 

Pour démarrer le serveur et le compilateur scss :

1. Entrer la commande "grunt".

2. Le serveur roule à l'adresse http://localhost:8080, le compilateur SASS roule en parallèle.
