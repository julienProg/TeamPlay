io.on('connection', function(socket) {

  socket.on('authRequest', function(user) {
    db.tables.users.find({
      name: user.name,
      password: user.password,
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      if (results.length === 1) {
        getFullUserInfo(user, function(results) {
          socket.emit('authResponse', {
            valid: true,
            data: results
          });
        });
      } else {
        socket.emit('authResponse', {
          valid: false,
          msg: "La combinaison fournie est invalide"
        });
      }
    });
  })

  socket.on('insertUser', function(user) {
    db.tables.users
      .find()
      .sort({
        userID: -1
      })
      .limit(1)
      .exec(function(err, results) {
        if (err) return console.error(err);
        var id = !results.length ? 1 : parseInt(results[0].userID) + 1;
        user.userID = id;
        user.dateCreated = Date.now();
        user.teamIDs = [];
        upsertUser(user, function() {
          getFullUserInfo(user, function(results) {
            socket.emit('authResponse', {
              valid: true,
              data: results
            });
          });
        });
      });
  });

  socket.on('insertTeam', function(team, user) {
    db.tables.teams
      .find()
      .sort({
        teamID: -1
      })
      .limit(1)
      .exec(function(err, results) {
        if (err) return console.error(err);
        var id = !results.length ? 1 : parseInt(results[0].teamID) + 1;
        team.teamID = id;
        team.userIDs = [];
        team.dashboardIDs = [];
        team.chatRoomIDs = [];
        team.calendarIDs = [];
        user.teamIDs.push(id);
        upsertUser(user, function(){
          upsertTeam(team, function(){
            getFullUserInfo(user, function(results){
              socket.emit('setData', results);
            });
          });
        });
      });
  });

  socket.on('insertChatRoom', function(chatRoom) {
    db.tables.chatRooms.find().sort({
      chatRoomID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var id = !results.length ? 1 : parseInt(results[0].chatRoomID) + 1;
      chatRoom.chatRoomID = id;
      chatRoom.messageIDs = [];
      upsertChatRoom(chatRoom);
    });
  });

  socket.on('insertChatMessage', function(chatMessage, chatRoomID) {
    db.tables.chatMessages.find().sort({
      chatMessageID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var chatMessageID = !results.length ? 1 : parseInt(results[0].chatMessageID) + 1;
      chatMessage.chatMessageID = chatMessageID;
      chatMessage.dateCreated = Date.now();
      pushChatMessageToChatRoom(chatRoomID, chatMessageID);
      upsertChatMessage(chatMessage);
      io.sockets.emit('addChatMessage', chatMessage, chatRoomID);
    });
  });

  socket.on('insertDashboard', function(dashboard) {
    db.tables.dashboards.find().sort({
      dashboardID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var id = !results.length ? 1 : parseInt(results[0].dashboardID) + 1;
      dashboard.dashboardID = id;
      dashboard.cardIDs = [];
      upsertDashboard(dashboard);
    });
  });

  socket.on('insertCard', function(card) {
    db.tables.cards.find().sort({
      cardID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var id = !results.length ? 1 : parseInt(results[0].cardID) + 1;
      card.cardID = id;
      card.dateCreated = Date.now();
      upsertCard(card);
    });
  });

  socket.on('insertCalendar', function(calendar) {
    db.tables.calendars.find().sort({
      calendarID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var id = !results.length ? 1 : parseInt(results[0].calendarID) + 1;
      calendar.calendarID = id;
      upsertCalendar(calendar);
    });
  });

  socket.on('insertCalendarEvent', function(calendarEvent) {
    db.tables.calendarEvents.find().sort({
      calendarEventID: -1
    }).limit(1).exec(function(err, results) {
      if (err) return console.error(err);
      var id = !results.length ? 1 : parseInt(results[0].calendarEventID) + 1;
      calendarEvent.calendarEventID = id;
      upsertCalendarEvent(calendarEvent);
    });
  });
});

function upsertUser(user, callback) {
  var query = {
    userID: user.userID
  };
  db.tables.users.findOneAndUpdate(query, user, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
    if( typeof callback === "function") callback();
  });
}

function upsertTeam(team, callback) {
  var query = {
    teamID: team.teamID
  };
  db.tables.teams.findOneAndUpdate(query, team, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
    if( typeof callback === "function") callback();
  });
}

function upsertDashboard(dashboard) {
  var query = {
    dashboardID: dashboard.dashboardID
  };
  db.tables.dashboards.findOneAndUpdate(query, dashboard, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
  });
}

function upsertCard(card) {
  var query = {
    cardID: card.cardD
  };
  db.tables.cards.findOneAndUpdate(query, card, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
  });
}

function upsertChatRoom(chatRoom) {
  var query = {
    chatRoomID: chatRoom.chatRoomID
  };
  db.tables.chatRooms.findOneAndUpdate(query, chatRoom, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
  });
}

function upsertChatMessage(chatMessage, callback) {
  var query = {
    chatMessageID: chatMessage.chatMessageID
  };
  db.tables.chatMessages.findOneAndUpdate(query, chatMessage, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
    //callback();
  });
}

function upsertCalendar(calendar) {
  var query = {
    calendarID: calendar.calendarID
  };
  db.tables.calendars.findOneAndUpdate(query, calendar, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
  });
}

function upsertCalendarEvent(calendarEvent) {
  var query = {
    calendarEvent: calendarEvent.calendarEventID
  };
  db.tables.calendarEvents.findOneAndUpdate(query, calendarEvent, {
    upsert: true
  }, function(err) {
    if (err) return console.error(err);
  });
}

function emitUsers(socket) {
  getUsers(function(results) {
    socket.emit('setUsers', results);
  })
}

function emitTeams(socket) {
  getTeams(function(results) {
    socket.emit('setTeams', results);
  })
}

function emitDashboards(socket) {
  getDashboards(function(results) {
    socket.emit('setDashboards', results);
  })
}

function emitCards(socket) {
  getCards(function(results) {
    socket.emit('setCards', results);
  })
}

function emitChatRoom(socket, chatRoomID) {
  getChatRoom(chatRoomID, function(results) {
    socket.emit('setChatRoom', results);
  })
}

function emitChatMessages(socket) {
  getChatMessages(function(results) {
    socket.emit('setChatMessages', results);
  })
}

function emitCalendars(socket) {
  getCalendars(function(results) {
    socket.emit('setCalendars', results);
  })
}

function emitCalendarEvents(socket) {
  getCalendarEvents(function(results) {
    socket.emit('setCalendarEvents', results);
  })
}

function getUsers(callback) {
  db.tables.users.find({}).exec(function(err, results) {
    if (err) console.error(err);
    callback(results);
  });
}

function getTeams(callback) {
  db.tables.teams.find({}).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function getDashboards(callback) {
  db.tables.dashboards.find({}).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function getCards(callback) {
  db.tables.cards.find({}).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function getChatRoom(chatRoomID, callback) {
  db.tables.chatRooms
    .aggregate({
      $unwind: "$chatMessageIDs"
    }, {
      $lookup: {
        from: "chatmessages",
        localField: "chatMessageIDs",
        foreignField: "chatMessageID",
        as: "chatMessage"
      }
    }, {
      $unwind: "$chatMessage",
    }, {
      $lookup: {
        from: "users",
        localField: "chatMessage.userID",
        foreignField: "userID",
        as: "userInfo"
      }
    })
    .exec(function(err, results) {
      console.log("results : ");
      console.log(results);
      callback(results);
    })
}

function getChatMessages(callback) {
  db.tables.chatMessages.aggregate({
    $lookup: {
      from: "users",
      localField: "userID",
      foreignField: "userID",
      as: "userInfo"
    }
  }).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function getCalendars(callback) {
  db.tables.calendars.find({}).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function getCalendarEvents(callback) {
  db.tables.calndarEvents.find({}).exec(function(err, results) {
    if (err) return console.error(err);
    callback(results);
  });
}

function pushChatMessageToChatRoom(chatRoomID, chatMessageID) {
  db.tables.chatRooms.update({
    chatRoomID: chatRoomID
  }, {
    $push: {
      chatMessageIDs: chatMessageID
    }
  }).exec();
}

function getFullUserInfo(u, callback) {
  db.tables.users
    .find({
      name: u.name,
      password: u.password,
    })
    .exec(function(err, user) {
      user = user[0];
      db.tables.users
        .find({}, { userID: 1, name: 1 }).lean()
        .exec(function(err, users) {
          if (err) console.error(err);
          db.tables.teams
            .find().lean()
            .exec(function(err, teams) {
              if (err) console.error(err);
              db.tables.dashboards
                .find().lean()
                .exec(function(err, dashboards) {
                  if (err) console.error(err);
                  db.tables.cards
                    .find().lean()
                    .exec(function(err, cards) {
                      if (err) console.error(err);
                      db.tables.chatRooms
                        .find().lean()
                        .exec(function(err, chatRooms) {
                          if (err) console.error(err);
                          db.tables.chatMessages
                            .find().lean()
                            .exec(function(err, chatMessages) {
                              if (err) console.error(err);
                              db.tables.calendars
                                .find().lean()
                                .exec(function(err, calendars) {
                                  if (err) console.error(err);
                                  db.tables.calendarEvents
                                    .find().lean()
                                    .exec(function(err, calendarEvents) {
                                      if (err) console.error(err);
                                      delete user.teamIDs;
                                      teams = filterTeams(user, teams);
                                      teams = populateTeams(teams, dashboards, cards, chatRooms, chatMessages, calendars, calendarEvents);
                                      teams = removeUselessProperties(teams);
                                      var data = {
                                        user: user,
                                        users: users,
                                        teams: teams,
                                      }
                                      callback(data);
                                    })
                                })

                            })
                        })
                    })
                })
            })
        });
    });
}

function filterTeams(user, teams) {
  var arr = [];
  var teamIDs = user.teamIDs;
  for (var i = 0; i < teamIDs.length; i++) {
    for (var j = 0; j < teams.length; j++) {
      if (teamIDs[i] === teams[j].teamID) {
        arr.push(teams[j]);
      }
    }
  }
  return arr;
}

function populateTeams(teams, dashboards, cards, chatRooms, chatMessages, calendars, calendarEvents) {
  for (var t = 0; t < teams.length; t++) { // For each team of this user
    teams[t].dashboards = [];
    teams[t].chatRooms = [];
    teams[t].calendars = [];

    for (var i = 0; i < teams[t].dashboardIDs.length; i++) //For each dashboardID in this team
      for (var j = 0; j < dashboards.length; j++) //For each dashboard in DB
        if (teams[t].dashboardIDs[i] === dashboards[j].dashboardID) //If match, add to object
          teams[t].dashboards.push(dashboards[j]);

    for (var i = 0; i < teams[t].dashboards.length; i++) { //For each dashboard in this team
      teams[t].dashboards[i].cards = [];
      for (var j = 0; j < teams[t].dashboards[i].cardIDs.length; j++) //For each cardID in this dashboard
        for (var k = 0; k < cards.length; k++) //For each card in DB
          if (teams[t].dashboards[i].cardIDs[j] === cards[k].cardID) //If match, add to object
            teams[t].dashboards[i].cards.push(cards[k]);
    }


    for (var i = 0; i < teams[t].chatRoomIDs.length; i++) //For each chatRoomID in this team
      for (var j = 0; j < chatRooms.length; j++) //For each chatRoom in DB
        if (teams[t].chatRoomIDs[i] === chatRooms[j].chatRoomID) //If match, add to object
          teams[t].chatRooms.push(chatRooms[j]);

    for (var i = 0; i < teams[t].chatRooms.length; i++) { //For each chatRoom in this team
      teams[t].chatRooms[i].chatMessages = [];
      for (var j = 0; j < teams[t].chatRooms[i].chatMessageIDs.length; j++) //For each chatMessageID in this chatRoom
        for (var k = 0; k < chatMessages.length; k++) //For each chatMessage in DB
          if (teams[t].chatRooms[i].chatMessageIDs[j] === chatMessages[k].chatMessageID) //If match, add to object
            teams[t].chatRooms[i].chatMessages.push(chatMessages[k]);
    }


    for (var i = 0; i < teams[t].calendarIDs.length; i++) //For each calendarIDs in this team
      for (var j = 0; j < calendars.length; j++) //For each calendar in DB
        if (teams[t].calendarIDs[i] === calendars[j].calendarID) //If match, add to object
          teams[t].calendars.push(calendars[j]);

    for (var i = 0; i < teams[t].calendars.length; i++) { //For each calendar in this team
      teams[t].calendars[i].calendarEvents = [];
      for (var j = 0; j < teams[t].calendars[i].calendarEventIDs.length; j++) //For each calendarEventID in this calendar
        for (var k = 0; k < calendarEvents.length; k++) //For each calendarEvent in DB
          if (teams[t].calendars[i].calendarEventIDs[j] === calendarEvents[k].calendarEventID) //If match, add to object
            teams[t].calendars[i].calendarEvents.push(calendarEvents[k]);
    }

  }
  return teams;
}

function removeUselessProperties(teams) {
  for (var t = 0; t < teams.length; t++) {
    for (var j = 0; j < teams[t].calendars.length; j++)
      delete teams[t].calendars[j].calendarEventIDs;

    for (var j = 0; j < teams[t].chatRooms.length; j++)
      delete teams[t].chatRooms[j].chatMessageIDs;

    for (var j = 0; j < teams[t].dashboards.length; j++)
      delete teams[t].dashboards[j].cardIDs;

    delete teams[t].calendarIDs;
    delete teams[t].chatRoomIDs;
    delete teams[t].dashboardIDs;
  }
  return teams;
}