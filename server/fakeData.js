use teamplay;
db.dropDatabase();
use teamplay;

db.users.insert({
  dateCreated: 1469300114126,
  email: "bernard.saucier@hotmail.com",
  name: "bernard",
  password: "saucier",
  teamIDs: [1, 2],
  userID: 1
})

db.users.insert({
  dateCreated: 1469300114126,
  email: "feel.the.bern@hotmail.com",
  name: "alex",
  password: "lemay",
  teamIDs: [1, 2],
  userID: 2
})

db.users.insert({
  dateCreated: 1469300114126,
  email: "Roger.saucier@hotmail.com",
  name: "Raoul",
  password: "cool",
  teamIDs: [],
  userID: 3
})

db.teams.insert({
  teamID: 1,
  name: "Dev Team",
  description: "equipe de dev",
  userIDs: [1, 2],
  dashboardIDs: [1],
  chatRoomIDs: [1, 2],
  calendarIDs: [1],
})

db.teams.insert({
  teamID: 2,
  name: "QAS Team",
  description: "assurance qualite",
  userIDs: [1, 2],
  dashboardIDs: [1],
  chatRoomIDs: [],
  calendarIDs: [1],
})

db.teams.insert({
  teamID: 3,
  name: "Autre",
  description: "loisir ou autres",
  userIDs: [1],
  dashboardIDs: [],
  chatRoomIDs: [],
  calendarIDs: [],
})

db.dashboards.insert({
  dashboardID: 1,
  name: "d1",
  cardIDs: [1, 2],
})

db.cards.insert({
  cardID: 1,
  name: "c1",
  content: "content!",
})

db.cards.insert({
  cardID: 2,
  name: "c2",
  content: "content!!!!!!!",
})

db.chatrooms.insert({
  chatRoomID: 1,
  name: "cr1",
  chatMessageIDs: [1, 2],
})

db.chatrooms.insert({
  chatRoomID: 2,
  name: "cr2",
  chatMessageIDs: [3, 4],
})

db.chatmessages.insert({
  chatMessageID: 1,
  content: "Yo Man !",
  userID: 1,
  dateCreated: 1469583604160,
})

db.chatmessages.insert({
  chatMessageID: 2,
  content: "Yo !",
  userID: 2,
  dateCreated: 1469583604160,
})

db.calendars.insert({
  calendarID: 1,
  name: "cal1",
  calendarEventIDs: [1, 2],
})

db.calendars.insert({
  calendarID: 2,
  name: "cal2",
  calendarEventIDs: [3, 4],
})

db.calendarevents.insert({
  calendarEventID: 1,
  name: "ce1",
  userID: 1,
  description: "desc1",
})

db.calendarevents.insert({
  calendarEventID: 2,
  name: "ce2",
  userID: 1,
  description: "desc2",
})

db.calendarevents.insert({
  calendarEventID: 3,
  name: "ce3",
  userID: 1,
  description: "desc3",
})

db.calendarevents.insert({
  calendarEventID: 4,
  name: "ce4",
  userID: 1,
  description: "desc4",
})
