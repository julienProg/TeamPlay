var db;

mongoose.connect("mongodb://localhost/teamplay");
db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var userSchema = mongoose.Schema({
  userID: Number,
  name: String,
  password: String,
  email: String,
  dateCreated: Number,
  teamIDs: Object
});

var teamSchema = mongoose.Schema({
  teamID: Number,
  name: String,
  description:String,
  userIDs: Object,
  dashboardIDs: Object,
  chatRoomIDs: Object,
  calendarIDs: Object
});

var dashboardSchema = mongoose.Schema({
  dashboardID: Number,
  name: String,
  cardIDs: Object
});

var cardSchema = mongoose.Schema({
  cardID: Number,
  dateCreated: Number,
  content: String,
  seenBy: Object
});

var chatRoomSchema = mongoose.Schema({
  chatRoomID: Number,
  name: String,
  chatMessageIDs: Object
});

var chatMessageSchema = mongoose.Schema({
  chatMessageID: Number,
  userID: Number,
  dateCreated: Number,
  content: String,
  seenBy: Object
});

var calendarSchema = mongoose.Schema({
  calendarID: Number,
  name: String,
  calendarEventIDs: Object
});

var calendarEventSchema = mongoose.Schema({
  calendarEventID: Number,
  userID: Number,
  date: Number,
  name: String,
  description: String,
  seenBy: Object
});

db.tables = {};
db.tables.users = mongoose.model('users', userSchema);
db.tables.teams = mongoose.model('teams', teamSchema);
db.tables.dashboards = mongoose.model('dashboards', dashboardSchema);
db.tables.cards = mongoose.model('cards', cardSchema);
db.tables.chatRooms = mongoose.model('chatrooms', chatRoomSchema);
db.tables.chatMessages = mongoose.model('chatmessages', chatMessageSchema);
db.tables.calendars = mongoose.model('calendars', calendarSchema);
db.tables.calendarEvents = mongoose.model('calendarevents', calendarEventSchema);
