function theTime() {
  var d = new Date();
  var hours = d.getHours();
  var minutes = d.getMinutes();
  if (minutes < 10) minutes = "0" + minutes;
  return hours + ":" + minutes;
}

var oldConsoleLog = console.log;
console.log = function() {
  Array.prototype.unshift.call(arguments, theTime() + " - ");
  oldConsoleLog.apply(this, arguments);
}
