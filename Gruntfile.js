module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concurrent: {
      target: {
        tasks: ['watch', 'nodemon'],
        options: {
          logConcurrentOutput: true,
        }
      }
    },
    sass: {
      dist: {
        files: {
          'public/css/style.css': 'public/scss/style.scss',
          'public/css/reset.css': 'public/scss/reset.scss',
        }
      }
    },
    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      }
    },
    nodemon: {
      dev: {
        script: 'server.js'
      }
    }
  });
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.registerTask('default', ['concurrent:target']);
}
